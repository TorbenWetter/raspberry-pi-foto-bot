"""
This script is intended to be executed by a cronjob (every minute)
"""

import os
from suntime import Sun
import pytz
from datetime import datetime
import math
from picamera import PiCamera
import requests
from time import sleep

photos_dir = 'photos'
home_lat, home_long = 50.019450, 8.212241
home_timezone = pytz.timezone('Europe/Berlin')


def get_sunset_time():
    return Sun(home_lat, home_long).get_sunset_time().astimezone(tz=home_timezone)


def get_current_time():
    return datetime.now(home_timezone).replace(second=0, microsecond=0)


def get_minutes_diff(time1, time2):
    return round((time2 - time1).total_seconds() / 60.0)


# This function is visualized here: https://www.geogebra.org/graphing/r7smdqer
def get_shutter_speed(minutes_diff):
    A1 = -1.6
    A2 = 1590
    dx = -7.5
    x0 = -11.0
    return A2 + ((A1 - A2) / (1 + math.exp((minutes_diff - x0) / dx)))


# Upload the photos to the transfer.sh server and delete them
def upload_photos(photo_paths):
    upload_urls = []
    for photo_path in photo_paths:
        with open(photo_path, 'rb') as data:
            file_name = os.path.basename(photo_path)
            r = requests.post('https://transfersh.com', files={
                file_name: data
            })
            upload_urls.append(r.text.strip())
        os.remove(photo_path)
    return upload_urls


# Send the upload_urls to my server
def send_urls_to_server(upload_urls, date_string, minutes_diff):
    requests.post('https://sunset.wetter.codes', json={
        'uploadUrls': upload_urls,
        'dateString': date_string,
        'minutesDiff': minutes_diff
    })


# The shutter speed value should be given in 1/shutter_speed of a second
def capture_photos(date_string, shutter_speed, minutes_diff):
    # Prepare the camera
    camera = PiCamera()
    camera.resolution = (2592, 1944)
    camera.framerate = 1
    camera.iso = 100
    camera.awb_mode = 'off'
    camera.awb_gains = (1.7, 1.3)
    camera.drc_strength = 'high'
    camera.start_preview()

    # Calculate different shutter speeds using various factors (the 1/.. is still not part of these values)
    photo_paths = []
    for factor in [0.0625, 0.25, 1.0, 4.0]:
        sleep(3)
        # The picamera shutter_speed value expects values in another format
        camera.shutter_speed = round(1000000 / (shutter_speed * factor))
        photo_path = photos_dir + '/' + date_string + '_' + str(shutter_speed * factor) + '.jpg'
        camera.capture(photo_path)
        photo_paths.append(photo_path)
    camera.stop_preview()
    del camera

    # Send the photos to my server (to process HDR there and to post the final photos)
    upload_urls = upload_photos(photo_paths)
    send_urls_to_server(upload_urls, date_string, minutes_diff)


if __name__ == '__main__':
    if not os.path.exists(photos_dir):
        os.makedirs(photos_dir)

    sunset_time = get_sunset_time()
    current_time = get_current_time()

    minutes_diff = get_minutes_diff(sunset_time, current_time)
    # Capture photos (with different exposures) every minute 30 minutes before and 29 minutes after the sunset
    if -30 <= minutes_diff <= 29:
        date_string = '{0:%Y}-{0:%m}-{0:%d}_{0:%H}:{0:%M}'.format(current_time)

        # Calculate the shutter speed using the current time (relative to the sunset) and capture photos
        shutter_speed = get_shutter_speed(minutes_diff)
        capture_photos(date_string, shutter_speed, minutes_diff)
